
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>5am Dashboard</title>
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="assets/images/favicons/site.webmanifest">

    <!-- plugin scripts -->


    <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:200,300,400,400i,500,600,700,800,900%7CSatisfy&display=swap" rel="stylesheet">

<link rel="stylesheet" href="<?= base_url(); ?>assets/css/animate.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/swiper.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/tripo-icons.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/vegas.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/nouislider.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/nouislider.pips.css">

    <!-- template styles -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/responsive.css">
    <style type="text/css">
        .searchbar{
        margin-bottom: auto;
        margin-top: auto;
        height: 60px;
        background-color:   #FF1493;
        border-radius: 30px;
        padding: 10px;
        }

        .search_input{
        color: white;
        border: 0;
        outline: 0;
        background: none;
        width: 0;
        caret-color:transparent;
        line-height: 40px;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_input{
        padding: 0 10px;
        width: 450px;
        caret-color:red;
        transition: width 0.4s linear;
        }

        .searchbar:hover > .search_icon{
        background: white;
        color: #e74c3c;
        }

        .search_icon{
        height: 40px;
        width: 40px;
        float: right;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        color:white;
        text-decoration:none;
        }
    </style>
    <style>
    img {
        width: 100%;
        height: auto;
    }
    </style>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>


	<style>
		.info { padding: 6px 8px; font: 14px/16px Arial, Helvetica, sans-serif; background: white; background: rgba(255,255,255,0.8); box-shadow: 0 0 15px rgba(0,0,0,0.2); border-radius: 5px; } .info h4 { margin: 0 0 5px; color: #777; }
		.legend { text-align: left; line-height: 18px; color: #555; } .legend i { width: 18px; height: 18px; float: left; margin-right: 8px; opacity: 0.7; }

	</style>
</head>
<body>
    <div class="preloader">
        <img src="<?= base_url(); ?>assets/images/loader.png" class="preloader__image" alt="">
    </div><!-- /.preloader -->
    <div class="page-wrapper">
        <div class="site-header__header-one-wrap">

            <header class="main-nav__header-one ">
                <nav class="header-navigation stricky">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="main-nav__logo-box">
                            <a href="" class="main-nav__logo">
                                <img src="<?= base_url(); ?>assets/images/logo_navbar.png" class="main-logo" width="123" alt="Awesome Image" />
                            </a>
                            <a href="#" class="side-menu__toggler"><i class="fa fa-bars"></i>
                                <!-- /.smpl-icon-menu --></a>
                        </div><!-- /.logo-box -->
                        <!-- Collect the nav links, forms, and other content for toggling -->

                        <div class="main-nav__right">
                          <div class="searchbar" >
                            <input class="search_input"type="text" name="" placeholder="Search...">
                            <a href="#" class="search_icon"><i class="fas fa-search"></i></a>
                          </div>
                        </div><!-- /.main-nav__right -->
                    </div>
                    <!-- /.container -->
                </nav>
            </header><!-- /.site-header -->
        </div><!-- /.site-header__header-one-wrap -->

<!-- ISI CONTENT -->

<section class="tour-two tour-list">
    <div class="container"><br>
      <div class="tour-sorter-one">
  <div class="tour-sorter-one__right">
                                     <a href="<?= base_url(); ?>portal/dashboard">Beranda</a>
                                    

                                 </div><!-- /.tour-sorter-one__right -->

                                    <div class="tour-sorter-one__right">
                                  
                                        <a href="<?= base_url(); ?>gis">BACK</a>
                                     

                                 </div><!-- /.tour-sorter-one__right -->

                             </div><!-- /.tour-sorter-one -->

      <center>  <p style="font-size: 50px">Badan Perencanaan dan Pembangunan Daerah Kota Malang</p>
                <p style="font-size: 35px">Bagian Wilayah Perencanaan (BWP) Blok </p>
                <p style="font-size: 25px; color: red;" >Sumber : https://si-petarungv2.malangkota.go.id/</p>




<div id="mapid" style="width: 100%; height: 1080px;"></div>
<script src="<?= base_url(); ?>assets/sources/bwp_blok.js"></script>
<script>

	var mymap = L.map('mapid').setView([-7.9786439,112.5967635], 13);
	var info = L.control();

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(mymap);

	var geojson;
	var statesData = {"type":"FeatureCollection",
					"features": data_main
				};

	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		this._div.innerHTML = '<h4 style=" font-family: Arial, Helvetica, sans-serif;">INFORMASI</h4><br />' +  (props ?
			'<b>' + props.nama + '</b>': 'Arahkan pada area untuk menegerti informasi');
	};

	info.addTo(mymap);

	function style(feature) {
		var properties = feature.properties;
		
		return {
			weight: properties.tebal,
			color: properties.warna
		};
	}

	function highlightFeature(e) {
		var layer = e.target;
        // console.log(layer);
		info.update(layer.feature.properties);
	}


	function zoomToFeature(e) {
		mymap.fitBounds(e.target.getBounds());
	}

	function onEachFeature(feature, layer) {
		layer.on({
			mouseover: highlightFeature,
			click: zoomToFeature
		});
	}

	geojson = L.geoJson(statesData, {
		// style: style,
		onEachFeature: onEachFeature
	}).addTo(mymap);


	L.circle([51.508, -0.11], 500, {
		color: 'red',
		fillColor: '#f03',
		fillOpacity: 0.5
	}).addTo(mymap).bindPopup("I am a circle.");


</script>


<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/TweenMax.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/swiper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/typed-2.0.11.js"></script>
    <script src="<?= base_url(); ?>assets/js/vegas.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/countdown.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/nouislider.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/isotope.js"></script>

    <!-- template scripts -->
    <script src="<?= base_url(); ?>assets/js/theme.js"></script>
</body>

</html>
