<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboardcustom extends CI_Model{
    
    public function get_all_dash($id_parent){
        $data = $this->db->query("SELECT * FROM fc_ms_smartcity.mn_dash md LEFT JOIN fc_ms_loc.ms_skpd skpd ON md.dinas_mn_dash = skpd.id_ms_skpd WHERE sha2(md.mn_dash_id, '256') = '".$id_parent."' and sts_active = '1' Order By row_mn_smart ASC");
        return $data->result();
    }

    public function get_each_dash($id_parent){
        $data = $this->db->query("SELECT * FROM fc_ms_smartcity.mn_dash md LEFT JOIN fc_ms_loc.ms_skpd skpd ON md.dinas_mn_dash = skpd.id_ms_skpd WHERE sha2(md.id_mn_dash, '256') = '".$id_parent."' and sts_active = '1'");

        // print_r($this->db->last_query());
        return $data->row_array();
    }

}
?>