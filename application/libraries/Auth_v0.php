 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_v0 {
    public function __construct(){
        $this->load->library('session');
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function set_session($param = null){
    	$status = false;
        if($param != null){
            // print_r($param);
    		$this->session->set_userdata("ih_mau_ngapain_x", $param);
            $status = true;
    	}
        return $status;
    }

    public function destroy_session($redirect = "login"){
        $this->session->sess_destroy();
        redirect($redirect);
    }

    public function get_session(){
        $data_session = array();
        if(isset($_SESSION["ih_mau_ngapain_x"])){
            $data_session = $_SESSION["ih_mau_ngapain_x"];
        }
        return $data_session;
    }

    public function auth_login(){
        if(isset($_SESSION["ih_mau_ngapain_x"])){
            if($_SESSION["ih_mau_ngapain_x"]["status_log"] == true){
                redirect(base_url()."beranda");
            }
        }
    }

    public function check_session_active_ad(){
        if(isset($_SESSION["ih_mau_ngapain_x"])){
            if($_SESSION["ih_mau_ngapain_x"]["status_log"] == true){
                // print_r("login");
                
            }else {
                redirect(base_url()."login");
            }
        }else {
            redirect(base_url()."login");
        }
    }
}