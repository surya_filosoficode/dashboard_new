<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">

<title>Login - 5AM DASHBOARD</title>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>template_login/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>template_login/css/fontawesome-all.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>template_login/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>template_login/css/style.css">
 <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
</head>

<body class="fullpage">
<div id="form-section" class="container-fluid signin">
    <a class="website-logo" href="https://malangkota.go.id/">
        <img class="logo" src="<?= base_url(); ?>template_login/images/logo.png" alt="Hostino">
    </a>
    <div class="menu-holder">
        <ul class="main-links">
           <!--  <li><a class="normal-link" href="signup.html">Don’t have an account?</a></li>
            <li><a class="sign-button" href="signup.html">Sign up <i class="hno hno-arrow-right"></i></a></li> -->
        </ul>
    </div>
    <div class="row">
        <div class="form-holder">
            <div class="signin-signup-form">
                <div class="form-items"><br><br>
                    <div class="form-title"><img src="<?= base_url(); ?>assets/images/logo_dashboard.png" style="width: 100%"  alt=""></div>
                    <div id="signinform">
                        <div class="form-text">
                            <input type="text" name="username" id="username" placeholder="username" required>
                            <p id="msg_username" style="color: red;"></p>
                        </div>
                        <div class="form-text">
                            <input type="password" name="password" id="password" placeholder="Password" required>
                            <p id="msg_password" style="color: red;"></p>
                        </div>
                        <div class="form-button">
                            <button id="login" type="submit" class="btn btn-default">Sign in <i class="hno hno-arrow-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="info-slider-holder">
            <div class="info-holder">
                <div class="img-text-slider">
                    <div>
                        <img src="<?= base_url(); ?>template_login/images/img-b1.png" alt="">
                        <p>Dashboard walikota merupakan sistem informasi yang terintegrasi dengan beberapa aplikasi milik OPD di Kota Malang menggunakan data visualisasi.</p>
                    </div>
                    <div>
                        <img src="<?= base_url(); ?>template_login/images/img-b2.png" alt="">
                        <p>Dashboard walikota merupakan sistem informasi yang terintegrasi dengan beberapa aplikasi milik OPD di Kota Malang menggunakan data visualisasi.</p>
                    </div>
                    <div>
                        <img src="<?= base_url(); ?>template_login/images/img-b3.png" alt="">
                        <p>Dashboard walikota merupakan sistem informasi yang terintegrasi dengan beberapa aplikasi milik OPD di Kota Malang menggunakan data visualisasi.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>template_login/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>template_login/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>template_login/js/slick.min.js"></script>
<script src="<?= base_url(); ?>template_login/js/main.js"></script>

<script type="text/javascript">
        $(document).ready(function() {

        });

        $("#login").click(function() {
            var data_main = new FormData();
            data_main.append('username'     , $("#username").val());
            data_main.append('password'     , $("#password").val());
            
            $.ajax({
                url: "<?= base_url(); ?>log/login/get_auth",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_login(res);
                    console.log(res);
                }
            });
        });

        function response_login(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            console.log(data_json);
            alert(main_msg.msg);

            if (main_msg.status) {
                window.location.href = window.location.href;
            } else {
                $("#msg_username").html(detail_msg.username);
                $("#msg_password").html(detail_msg.password);
            }
        }
    </script>
</body>
</html>
