<?php
    $url_main_app = base_url()."beranda";
    $url_sub_app = base_url()."beranda/sub-menu/";

    $url_content_app = base_url()."beranda/content/";

    $main_controller = base_url()."dashboard";

    $path_img = "https://dashboard.malangkota.go.id/app_admin/assets/dash/img/icon/";


    $str_list_menu = "";
    $str_list_content = "";

    $id_mn_dash = "";
    $mn_dash_id = "";
    $parent_mn_dash = "";
    $nama_mn_dash = "";
    $ket_mn_dash = "";
    $sts_mn_dash = "";
    $slug_mn_dash = "";
    $link_mn_dash = "";
    $icon_mn_dash = "";
    $seo_mn_dash = "";
    $content_mn_dash = "";
    $sts_active = "";


    $sumber_mn_dash = "";
    $dinas_mn_dash = "";
    $nama_ms_skpd = "";


    if(isset($list_data)){
        if($list_data){
            $id_mn_dash = $list_data["id_mn_dash"];
            $mn_dash_id = $list_data["mn_dash_id"];
            $parent_mn_dash = $list_data["parent_mn_dash"];
            $nama_mn_dash = $list_data["nama_mn_dash"];
            $ket_mn_dash = $list_data["ket_mn_dash"];
            $sts_mn_dash = $list_data["sts_mn_dash"];
            $slug_mn_dash = $list_data["slug_mn_dash"];
            $link_mn_dash = $list_data["link_mn_dash"];
            $icon_mn_dash = $list_data["icon_mn_dash"];
            $seo_mn_dash = $list_data["seo_mn_dash"];
            $sts_active = $list_data["sts_active"];
            $seo_mn_dash = $list_data["seo_mn_dash"];
            $content_mn_dash = $list_data["content_mn_dash"];

            $sumber_mn_dash = $list_data["sumber_mn_dash"];
            $dinas_mn_dash = $list_data["dinas_mn_dash"];
            $nama_ms_skpd = $list_data["nama_ms_skpd"];

        }
    }


    $title_parent = "";
    if(isset($parent)){
        if($parent){
            $title_parent = $parent["nama_mn_dash"];
        }
    }


    $str_bchum = "<nav>
                    <ol class=\"breadcrumb-list\">
                      <li class=\"breadcrumb-item\"><a href=\"".$url_main_app."\">Beranda</a></li>";
    if(isset($patern)){
      if($patern){
        foreach ($patern as $key => $value) {
          // print_r($value);
          $id_mn_dash_patern = $value["id_mn_dash"];
          $nama_mn_dash_patern = str_replace("<br>", " ", $value["nama_mn_dash"]);
          $slug_mn_dash_patern = $value["slug_mn_dash"];

          $str_bchum .= "<li class=\"breadcrumb-item active\"><a href=\"".$url_sub_app.hash("sha256", $id_mn_dash_patern)."/?slug=".$slug_mn_dash_patern."\">".$nama_mn_dash_patern."</a></li>";          
        }
      }
    }

    if(isset($parent)){
      if($parent){
        // print_r($parent);
          $id_mn_dash_parent = $parent["id_mn_dash"];
          $nama_mn_dash_parent = str_replace("<br>", " ", $parent["nama_mn_dash"]);
          $slug_mn_dash_parent = $parent["slug_mn_dash"];
        $str_bchum .= "<li class=\"breadcrumb-item active\"><a href=\"javascript:void(0)\">".$nama_mn_dash_parent."</a></li>";
      }
    }

    $str_bchum .= "</ol>
              </nav>";
?>

<!DOCTYPE html>
<html lang="en">


<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$title?></title>

    <meta name="description" content="<?=$seo_description?>" />
    <meta name="keywords" content="<?=$seo_keywords?>" />

    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="assets/images/favicons/site.webmanifest">

    <!-- plugin scripts -->


    <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:200,300,400,400i,500,600,700,800,900%7CSatisfy&display=swap" rel="stylesheet">


    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/animate.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/swiper.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/tripo-icons.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/vegas.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/nouislider.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/nouislider.pips.css">

    <!-- template styles -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/responsive.css">

    <style type="text/css">
      img {
          width: 100%;
          height: auto;
      }

      .button {
          background-color: #40b9eb;
          border: none;
          color: white;
          padding: 15px 32px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin: 4px 2px;
          cursor: pointer;
      }

     .breadcrumb-list {
     display: flex;
     flex-wrap: wrap;
     padding: 0;
     margin: 1rem 0 0 0;
     list-style: none;
     li {
       font-size: 0.85rem;
       letter-spacing: 0.125rem;
       text-transform: uppercase;
     }
   }

   .breadcrumb-item {
     &.active {
       color: $black;
     }
     + .breadcrumb-item {
       &::before {
         content: '/';
         display: inline-block;
         padding: 0 0.5rem;
         color: $lightergray;
       }
     }
   }
   </style>
   
</head>

<body>
    <div class="preloader">
        <img src="<?= base_url(); ?>assets/images/loader.png" class="preloader__image" alt="">
    </div><!-- /.preloader -->
    <div class="page-wrapper">
        <div class="site-header__header-one-wrap">

            <header class="main-nav__header-one ">
                <nav class="header-navigation stricky">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="main-nav__logo-box">
                            <a href="<?= base_url(); ?>dashboard" class="main-nav__logo">
                                <img src="<?= base_url(); ?>assets/images/logo_navbar.png" class="main-logo" width="123" alt="Awesome Image" />
                            </a>
                            <a href="#" class="side-menu__toggler"><i class="fa fa-bars"></i>
                                <!-- /.smpl-icon-menu --></a>
                        </div><!-- /.logo-box -->
                        <!-- Collect the nav links, forms, and other content for toggling -->

                        <div class="main-nav__right">
                          
                         <!--  <div class="searchbar" >
                            <input class="search_input"type="text" name="" placeholder="Search...">
                            <a href="#" class="search_icon"><i class="fas fa-search"></i></a>
                          </div> -->
                            <a class="button" href="<?= base_url(); ?>log/logout">LOGOUT</a>
                        </div><!-- /.main-nav__right -->
                    </div>
                    <!-- /.container -->
                </nav>
            </header><!-- /.site-header -->
        </div><!-- /.site-header__header-one-wrap -->

<!-- ISI CONTENT -->

        <section>
            <div class="container"><br>
              <!-- breadcrumb -->
              <!-- <nav>
                <ol class="breadcrumb-list">
                  <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                  <li class="breadcrumb-item"><a href="#">Main Sub</a></li>
                  <li class="breadcrumb-item active">Content</li>
                </ol>
              </nav> -->

              <?=$str_bchum?>
              </div><br>
              <!-- end-breadcrumb -->
                <center>
                    <p style="font-size: 50px"><?=ucwords($nama_ms_skpd)?></p><!-- Nama OPD terkait -->
                    <p style="font-size: 35px">Data <?=$nama_mn_dash?> </p> <!-- Judul Child -->
                    <p style="font-size: 25px; color: red;" >Sumber : <?=$sumber_mn_dash?></p> <!-- Sumber -->

                    <?=$content_mn_dash?>
            </div> </center>
            </div><!-- /.container -->
            </div><!-- /.container -->
        </section><!-- /.destinations-two -->



        <div class="site-footer__bottom">
          <div class="container">
              <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
              Pemerintah Kota Malang</a></p>
              <div class="site-footer__social">
                  <a href="https://www.facebook.com/pemkot.malang/"><i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square --></a>
                  <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i><!-- /.fab fa-twitter --></a>
                  <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i><!-- /.fab fa-instagram --></a>
              </div><!-- /.site-footer__social -->
          </div><!-- /.container -->
        </div><!-- /.site-footer__bottom -->

    </div><!-- /.page-wrapper -->


    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <div class="side-menu__block">


        <div class="side-menu__block-overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.side-menu__block-overlay -->
        <div class="side-menu__block-inner ">
            <div class="side-menu__top justify-content-end">

                <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="<?php base_url(); ?>assets/images/shapes/close-1-1.png" alt=""></a>
            </div><!-- /.side-menu__top -->


            <nav class="mobile-nav__container">
                <!-- content is loading via js -->
            </nav>
            <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
            <div class="side-menu__content">
              <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
              Pemerintah Kota Malang</a></p>
                <div class="side-menu__social">
                  <a href="https://www.facebook.com/pemkot.malang/"><i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square --></a>
                  <a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i><!-- /.fab fa-twitter --></a>
                  <a href="https://www.instagram.com/pemkotmalang/"><i class="fab fa-instagram"></i><!-- /.fab fa-instagram --></a>
                </div>
            </div><!-- /.side-menu__content -->
        </div><!-- /.side-menu__block-inner -->
    </div><!-- /.side-menu__block -->

    <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/TweenMax.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/swiper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/typed-2.0.11.js"></script>
    <script src="<?= base_url(); ?>assets/js/vegas.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/countdown.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/nouislider.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/isotope.js"></script>

    <!-- template scripts -->
    <script src="<?= base_url(); ?>assets/js/theme.js"></script>
</body>

</html>
