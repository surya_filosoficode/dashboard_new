<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_main extends CI_Model{

	public function select_admin($where, $where_or){
		$this->db->select("id_appl_user, appl_m_unit_kerja_id, appl_m_tipe_user_id, nama_user, kd_nm_user, username, nip, email, no_hp, pass, auto_reset_pass, unit_kerja, wilayah, description, disabled, theme, created_by, creation_date, last_updated_by, last_updated_date");

        $this->db->or_where("(username = '".$where_or["username"]."'");
        $this->db->or_where("email = '".$where_or["email"]."')");
        // $this->db->where($where);

        $data = $this->db->get_where("appl_user", $where)->row_array();
        return $data;
	}

    public function select_admin_all($where){
        $this->db->select("id_appl_user, appl_m_unit_kerja_id, appl_m_tipe_user_id, nama_user, kd_nm_user, username, nip, email, no_hp, pass, auto_reset_pass, unit_kerja, wilayah, description, disabled, theme, created_by, creation_date, last_updated_by, last_updated_date");
        $data = $this->db->get_where("appl_user", $where)->result();
        return $data;
    }
    

}
?>