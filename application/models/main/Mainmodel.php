<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmodel extends CI_Model{

    public $db_custom;

    public function __construct(){
        parent::__construct(); 
        
        $this->db_custom = $this->load->database('db_dashboard', TRUE);
    }
    
    public function get_data_all($table){
    	$data = $this->db_custom->get($table);
    	return $data->result();
    }

    public function get_data_all_where($table, $where){
    	$data = $this->db_custom->get_where($table, $where);
    	return $data->result();
    }

    public function get_data_each($table, $where){
    	$data = $this->db_custom->get_where($table, $where);
    	return $data->row_array();
    }

    public function insert_data($table, $data){
    	$insert = $this->db_custom->insert($table, $data);
    	return $insert;
    }

    public function update_data($table, $set, $where){
    	$update = $this->db_custom->update($table, $set, $where);
    	return $update;
    }

    public function delete_data($table, $where){
    	$delete = $this->db_custom->delete($table, $where);
    	return $delete;
    }

    public function get_data_all_where_order_row($table, $where){
        $this->db_custom->order_by("row_mn_smart", "ASC");
    	$data = $this->db_custom->get_where($table, $where);
    	return $data->result();
    }

}
?>