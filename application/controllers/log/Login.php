<?php
class Login extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
		$this->load->model('login/admin_main', 'am');

		$this->load->library("response_message");
		$this->load->library("Auth_v0");

    $this->auth_v0->auth_login();
  }

  function index(){
    $this->load->view('dashboard_view/login_view');
  }

  private function val_form_log(){
    $config_val_input = array(
            array(
                'field'=>'username',
                'label'=>'username',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'password',
                'label'=>'password',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            )
        );
    $this->form_validation->set_rules($config_val_input); 
    return $this->form_validation->run();
  }

  public function get_auth(){
      $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
      $msg_detail = array("username" => "",
                          "password" => "");
      if($this->val_form_log()){

        
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $where = array(
              'pass' => hash("sha256", $password)
        );

        $where_or = array(
            'username' => $username,
            'nip' => $username,
            'email' => $username,
            'no_hp' => $username,
            'pass' => $username
        );
      
        $cek = $this->am->select_admin($where, $where_or);

        // print_r($where);
        
        if($cek){
          // print_r("ok");
            $cek["status_log"] = true;
            if($this->auth_v0->set_session($cek)){
                $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC"));
            }
        }

        // print_r($cek);

      }else {
        $msg_detail["username"] = form_error("username");
        $msg_detail["password"] = form_error("password");

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
      }
      // print_r($cek);

      $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
      print_r(json_encode($msg_array));
  }

  public function check_session(){
      print_r($_SESSION);
  }
}
