<?php
class Dashboard extends CI_Controller
{
    public $title;
    public $seo_description;
    public $seo_keywords;

    public $tbl_main;

    function __construct(){
        parent::__construct();
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('main/dashboardcustom', 'dm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");

        $this->auth_v0->check_session_active_ad();

        $this->title = "5AM Dashboard";
        $this->seo_description = "Website Dashboard Kota Malang, Malang Kota Cerdas, Malang Smartcity";
        $this->seo_keywords = "malangkota.go.id, portal malang Dashboard, Dashboard kota malang";
        
        $this->tbl_main = "mn_dash";


    }

    function index(){
        $data["page"] = "p_user_profil";
        $data["title"] = $this->title;
        $data["seo_description"] = $this->seo_description;
        $data["seo_keywords"] = $this->seo_keywords;

        $data["list_data"] = $this->mm->get_data_all_where_order_row(
                                    $this->tbl_main,
                                    ["mn_dash_id"=>"0", "sts_active"=>"1"]
                                );

		$this->load->view('index', $data);
    }

    function index_sub($id_parent){

        $data["page"] = "p_user_profil";
        $data["title"] = $this->title;
        $data["seo_description"] = $this->seo_description;
        $data["seo_keywords"] = $this->seo_keywords;

        $data["patern"] = [];

        $data_perent = $this->dm->get_each_dash($id_parent);
        
        $data["parent"] = $data_perent;
        if($id_parent != "0"){
            $pattern = $data_perent["parent_mn_dash"];
            
            $data["seo_keywords"] = $data_perent["seo_mn_dash"];
            $arr_parent = json_decode($pattern);
            
            foreach ($arr_parent as $key => $value) {
                $tmp_val = $this->mm->get_data_all_where($this->tbl_main, ["id_mn_dash"=>$value]);
                $data["patern"][$key] = $tmp_val;   
            }
        }

        $data["list_data"] = $this->dm->get_all_dash(
                                    $id_parent
                                );

        
        // print_r("<pre>");
        // print_r($data);
        // die();
		$this->load->view('index_sub', $data);

    }

    function index_content($id_data){

        $data["page"] = "p_user_profil";
        $data["title"] = $this->title;
        $data["seo_description"] = $this->seo_description;
        $data["seo_keywords"] = $this->seo_keywords;

        $data["patern"] = [];

        $data_perent = $this->mm->get_data_each($this->tbl_main, ["sha2(id_mn_dash, '256') = "=>$id_data]);
        $data["parent"] = $data_perent;
        if($id_data != "0"){
            $pattern = $data_perent["parent_mn_dash"];
            
            $data["seo_keywords"] = $data_perent["seo_mn_dash"];
            $arr_parent = json_decode($pattern);
            
            foreach ($arr_parent as $key => $value) {
                $tmp_val = $this->mm->get_data_each($this->tbl_main, ["id_mn_dash"=>$value]);
                $data["patern"][$key] = $tmp_val;   
            }
        }

        $data["list_data"] = $this->dm->get_each_dash(
                                    $id_data
                                );

        
        // print_r();
        // print_r("<pre>");
        // print_r($data);
        // die();
        $this->load->view('index_content', $data);

    }
}
